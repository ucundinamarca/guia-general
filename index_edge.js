/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage_1',
                            type: 'group',
                            rect: ['-67', '0', '1125', '825', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo',
                                type: 'image',
                                rect: ['67px', '0px', '1020px', '780px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Fondo-Pantalla1.jpg",'0px','0px']
                            },
                            {
                                id: 'alpha',
                                type: 'image',
                                rect: ['84px', '32px', '952px', '724px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"formas-pantalla12.png",'0px','0px']
                            },
                            {
                                id: 'libro',
                                type: 'image',
                                rect: ['106px', '492px', '281px', '119px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_31.png",'0px','0px']
                            },
                            {
                                id: 'guia',
                                type: 'image',
                                rect: ['56px', '41px', '325px', '92px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_262.png",'0px','0px']
                            },
                            {
                                id: 'edificio',
                                type: 'image',
                                rect: ['68px', '251px', '1020px', '233px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_25.png",'0px','0px']
                            },
                            {
                                id: 'avatars',
                                type: 'group',
                                rect: ['94', '362', '976', '401', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'avatar01',
                                    type: 'image',
                                    rect: ['521px', '0px', '133px', '184px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_24.png",'0px','0px'],
                                    userClass: "avatars"
                                },
                                {
                                    id: 'avatar02',
                                    type: 'image',
                                    rect: ['654px', '50px', '82px', '198px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_23.png",'0px','0px'],
                                    userClass: "avatars"
                                },
                                {
                                    id: 'avatar03',
                                    type: 'image',
                                    rect: ['234px', '98px', '133px', '169px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_22.png",'0px','0px'],
                                    userClass: "avatars"
                                },
                                {
                                    id: 'avatar04',
                                    type: 'image',
                                    rect: ['371px', '4px', '55px', '189px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_21.png",'0px','0px'],
                                    userClass: "avatars"
                                },
                                {
                                    id: 'avatar05',
                                    type: 'image',
                                    rect: ['154px', '6px', '67px', '209px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px'],
                                    userClass: "avatars"
                                },
                                {
                                    id: 'avatar06',
                                    type: 'image',
                                    rect: ['893px', '52px', '83px', '197px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px'],
                                    userClass: "avatars"
                                },
                                {
                                    id: 'avatar07',
                                    type: 'image',
                                    rect: ['0px', '10px', '75px', '199px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_18.png",'0px','0px'],
                                    userClass: "avatars"
                                },
                                {
                                    id: 'avatar08',
                                    type: 'image',
                                    rect: ['817px', '111px', '80px', '215px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_17.png",'0px','0px'],
                                    userClass: "avatars"
                                },
                                {
                                    id: 'avatar09',
                                    type: 'image',
                                    rect: ['414px', '168px', '125px', '192px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_162.png",'0px','0px'],
                                    userClass: "avatars"
                                },
                                {
                                    id: 'avatar10',
                                    type: 'image',
                                    rect: ['545px', '258px', '191px', '143px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_152.png",'0px','0px'],
                                    userClass: "avatars"
                                },
                                {
                                    id: 'avatar11',
                                    type: 'image',
                                    rect: ['737px', '113px', '72px', '212px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_13.png",'0px','0px'],
                                    userClass: "avatars"
                                },
                                {
                                    id: 'avatar12',
                                    type: 'image',
                                    rect: ['87px', '99px', '74px', '202px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_12.png",'0px','0px'],
                                    userClass: "avatars"
                                },
                                {
                                    id: 'avatar13',
                                    type: 'image',
                                    rect: ['187px', '239px', '112px', '136px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"girl_22.png",'0px','0px'],
                                    userClass: "avatars"
                                }]
                            },
                            {
                                id: 'flor1',
                                type: 'image',
                                rect: ['0px', '565px', '280px', '254px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_41.png",'0px','0px'],
                                userClass: "flores"
                            },
                            {
                                id: 'flor2',
                                type: 'image',
                                rect: ['824px', '573px', '301px', '252px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_422.png",'0px','0px'],
                                userClass: "flores",
                                transform: [[],[],[],['1.01','1.01']]
                            },
                            {
                                id: 'btn_1',
                                type: 'image',
                                rect: ['104px', '213px', '129px', '130px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_1.png",'0px','0px'],
                                userClass: "btn"
                            },
                            {
                                id: 'btn_2',
                                type: 'image',
                                rect: ['323px', '183px', '129px', '130px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_2.png",'0px','0px'],
                                userClass: "btn"
                            },
                            {
                                id: 'btn_3',
                                type: 'image',
                                rect: ['504px', '148px', '129px', '130px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_3.png",'0px','0px'],
                                userClass: "btn"
                            },
                            {
                                id: 'btn_4',
                                type: 'image',
                                rect: ['724px', '192px', '129px', '130px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_4.png",'0px','0px'],
                                userClass: "btn"
                            },
                            {
                                id: 'btn_5',
                                type: 'image',
                                rect: ['941px', '204px', '129px', '130px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_5.png",'0px','0px'],
                                userClass: "btn"
                            },
                            {
                                id: 'label_1',
                                type: 'image',
                                rect: ['95px', '171px', '148px', '42px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"label_1.png",'0px','0px'],
                                userClass: "label"
                            },
                            {
                                id: 'label_2',
                                type: 'image',
                                rect: ['301px', '133px', '173px', '83px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"label_2.png",'0px','0px'],
                                userClass: "label"
                            },
                            {
                                id: 'label_3',
                                type: 'image',
                                rect: ['467px', '94px', '219px', '46px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"label_3.png",'0px','0px'],
                                userClass: "label"
                            },
                            {
                                id: 'label_4',
                                type: 'image',
                                rect: ['699px', '150px', '168px', '50px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"label_4.png",'0px','0px'],
                                userClass: "label"
                            },
                            {
                                id: 'label_5',
                                type: 'image',
                                rect: ['923px', '165px', '152px', '48px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"label_5.png",'0px','0px'],
                                userClass: "label"
                            }]
                        },
                        {
                            id: 'stage_2',
                            type: 'group',
                            rect: ['-75', '0', '1122', '825', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo2',
                                type: 'image',
                                rect: ['75px', '0px', '1020px', '780px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Fondo-Pantalla2.jpg",'0px','0px']
                            },
                            {
                                id: 'alpha2',
                                type: 'image',
                                rect: ['76px', '41px', '951px', '719px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"formas-pantalla2.png",'0px','0px']
                            },
                            {
                                id: 'len',
                                type: 'group',
                                rect: ['404', '109px', '344', '242', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'len1',
                                    type: 'group',
                                    rect: ['62px', '0px', '227', '217', 'auto', 'auto'],
                                    userClass: "len",
                                    c: [
                                    {
                                        id: 'btn_12',
                                        type: 'image',
                                        rect: ['54px', '0px', '129px', '130px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn_1.png",'0px','0px']
                                    },
                                    {
                                        id: 'l_1',
                                        type: 'image',
                                        rect: ['3px', '155px', '227px', '62px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"l_1.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'len2',
                                    type: 'group',
                                    rect: ['51px', '0px', '249', '242', 'auto', 'auto'],
                                    userClass: "len",
                                    c: [
                                    {
                                        id: 'l2',
                                        type: 'image',
                                        rect: ['0px', '158px', '249px', '84px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"l2.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn_22',
                                        type: 'image',
                                        rect: ['61px', '0px', '129px', '130px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn_2.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'len3',
                                    type: 'group',
                                    rect: ['0px', '5px', '344', '217', 'auto', 'auto'],
                                    userClass: "len",
                                    c: [
                                    {
                                        id: 'l3',
                                        type: 'image',
                                        rect: ['0px', '150px', '344px', '67px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"l3.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn_32',
                                        type: 'image',
                                        rect: ['112px', '0px', '129px', '130px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn_3.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'len4',
                                    type: 'group',
                                    rect: ['54px', '5px', '237', '218', 'auto', 'auto'],
                                    userClass: "len",
                                    c: [
                                    {
                                        id: 'l_4',
                                        type: 'image',
                                        rect: ['0px', '148px', '237px', '70px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"l_4.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn_42',
                                        type: 'image',
                                        rect: ['54px', '0px', '129px', '130px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn_4.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'len5',
                                    type: 'group',
                                    rect: ['63px', '5px', '201', '218', 'auto', 'auto'],
                                    userClass: "len",
                                    c: [
                                    {
                                        id: 'l5',
                                        type: 'image',
                                        rect: ['10px', '145px', '201px', '73px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"l5.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn_52',
                                        type: 'image',
                                        rect: ['49px', '0px', '129px', '130px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn_5.png",'0px','0px']
                                    }]
                                }]
                            },
                            {
                                id: 'hoja_1',
                                type: 'image',
                                rect: ['0px', '571px', '280px', '254px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_41.png",'0px','0px'],
                                userClass: "flores"
                            },
                            {
                                id: 'hoja_2',
                                type: 'image',
                                rect: ['821px', '573px', '301px', '252px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_42.png",'0px','0px'],
                                userClass: "flores"
                            },
                            {
                                id: 'Group2',
                                type: 'group',
                                rect: ['208px', '312', '735px', '328', 'auto', 'auto'],
                                overflow: 'auto',
                                userClass: "contenedor",
                                c: [
                                {
                                    id: 'texto',
                                    type: 'text',
                                    rect: ['0px', '0px', '688px', '328px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px;\">​</p>",
                                    font: ['Arial, Helvetica, sans-serif', [12, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                }]
                            },
                            {
                                id: 'close',
                                type: 'image',
                                rect: ['984px', '7px', '107px', '107px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"close.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '780px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '306px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1020px', '780px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [
                        [
                            "eid13",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${hoja_2}",
                            [59,77],
                            [59,77],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid35",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${hoja_2}",
                            [59,77],
                            [59,77],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid36",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${hoja_2}",
                            [59,77],
                            [59,77],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid37",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${hoja_2}",
                            [59,77],
                            [59,77],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid38",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${hoja_2}",
                            [59,77],
                            [59,77],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid39",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${hoja_2}",
                            [59,77],
                            [59,77],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid1",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${flor2}",
                            [59,78],
                            [59,78],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid40",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${flor2}",
                            [59,78],
                            [59,78],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid41",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${flor2}",
                            [59,78],
                            [59,78],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid42",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${flor2}",
                            [59,78],
                            [59,78],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid43",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${flor2}",
                            [59,78],
                            [59,78],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid44",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${flor2}",
                            [59,78],
                            [59,78],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid14",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${hoja_1}",
                            [31,78],
                            [31,78],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid45",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${hoja_1}",
                            [31,78],
                            [31,78],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid46",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${hoja_1}",
                            [31,78],
                            [31,78],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid47",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${hoja_1}",
                            [31,78],
                            [31,78],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid48",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${hoja_1}",
                            [31,78],
                            [31,78],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid49",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${hoja_1}",
                            [31,78],
                            [31,78],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid2",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${flor1}",
                            [27,77],
                            [27,77],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid50",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${flor1}",
                            [27,77],
                            [27,77],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid51",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${flor1}",
                            [27,77],
                            [27,77],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid52",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${flor1}",
                            [27,77],
                            [27,77],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid53",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${flor1}",
                            [27,77],
                            [27,77],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid54",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${flor1}",
                            [27,77],
                            [27,77],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");
