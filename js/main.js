
var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Guia General",
    autor: "Edilson Laverde Molina",
    date:  "07/07/2021",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios=[
    {url:"sonidos/click.mp3",      name:"clic"      }
];

var textos=[
    {
        "tittle"  :"Bienvenida",
        "contents":`
            <p>Apreciado estudiante, bienvenido al diplomado <b>Diversidad como Propósito Educativo,</b> diseñado en el marco del Modelo Educativo Digital Transmoderno de nuestra universidad.</p>
            <p>Está propuesta formativa está orientada a generar conocimiento de forma práctica y experiencial, con la intención de motivar la construcción y desarrollo de herramientas pedagógicas, que permitan integrar posturas, actitudes y concepciones desde el reconocimiento de la diversidad como propósito educativo y así dar sentido a la explicación y apropiación de acciones educativas inclusivas en el ámbito de la Educación Superior (ES).</p>
            <p>Por lo anterior, el abordaje de los procesos educativos inclusivos, buscan enfocar la atención a la diversidad, con miras a la detección de fortalezas y oportunidades de mejoramiento, que promuevan el respeto a la diversidad, la equidad y la generación de oportunidades, para así hacer realidad la educación para la vida, siendo esta generadora y visibilizadora de la diferencia como condición inherente al ser humano.</p>
        `
    },{
        "tittle"  :"Presentación del curso",
        "contents":`
            <p>El preámbulo de la declaración universal de los Derechos Humanos afirma la importancia de reconocernos como una familia humana. Esta connotación, sitúa dos características principales:</p>
            <ul>
                <li>La primera habla de reconocimiento, lo que significa cuestiones y reflexiones desde la perspectiva de los sujetos sobre las fortalezas, cualidades, capacidades y recursos que poseen. Esto implica dar a los individuos, una construcción de identidad y de sujeto autónomo, crítico, transformador y solidario.</li>
                <li>La segunda, conduce a una comprensión del término <b>“familia”,</b> la cual debe ser entendida como unidad, nicho, tejido y red con dinámicas, relaciones y concepciones de vida, con intereses y singularidades propios de cada sujeto. </li>
            </ul>
            <p>Lo anterior, lleva a las concepciones y/o terminologías tales como: enfoque de derechos, enfoque diferencial, culturas e interculturalidad, subjetividad e intersubjetividad, otredad y alteridad, que se abordan desde las comprensiones y definiciones de diversidad.</p>
            <p>La Universidad de Cundinamarca con su Modelo Educativo Digital Transmoderno, busca educar para la vida, los valores democráticos, la civilidad y la libertad, valores necesarios para establecer una convivencia sana, que dé cuenta del respeto y la equidad, reconociendo el diálogo como elemento constructivo. En este sentido, (Anijovich, 2007, p9.)  señala: <i>“la diversidad presenta múltiples retos, entre los cuales podemos destacar los relacionados con tomar decisiones políticas y pedagógicas: implica debatir y lograr consensos acerca de qué es lo común que todos los estudiantes deben aprender y, utilizar estrategias diferenciadoras”.</i></p>
            <p>Por lo tanto, los docentes como sujetos educativos, políticos y sociales, son transformadores de sus propios discursos y generadores de cambios que posibilitan el aprendizaje de sus estudiantes, respetando y valorando a cada sujeto desde sus capacidades, cualidades, habilidades y contextos, asumiendo la educación como oportunidad de cambio. Esto permite que se establezcan políticas, culturas y prácticas educativas inclusivas, a partir de las cuales, los estudiantes desarrollen competencias sociales, actitudinales y laborales, que les permitan reconocer la diversidad como una cualidad y característica nativamente humana, que se convierte en oportunidad de crecimiento profesional y personal que contribuye a la construcción y reconocimiento de una sociedad plural a todas luces incluyente.</p>
            <p>Esperamos que este proceso redunde en beneficios para su vida y fortalezca su ámbito personal, profesional y laboral.</p>
        `
    },
    {
        "tittle"  :"REA GENERAL",
        "contents":`
            <p>Transformar el paradigma de la diversidad como propósito educativo mediante la explicación, apropiación e implementación de acciones educativas inclusivas en el ámbito universitario.</p>
            <h1>REA ESPECÍFICOS</h1>
            <ul>
                <li>Implementar lo estudiado sobre diversidad e inclusión a partir del análisis, al contexto educativo Universitario.</li>
                <li>Implementar las características de la diversidad a partir de la cultura e interculturalidad, subjetividad e intersubjetividad, otredad y alteridad como oportunidad de la educación inclusiva y su trascendencia en la cotidianidad al interior de nuestra universidad.</li>
                <li>Aplicar estrategias de atención educativa a la diversidad a partir nuevas propuestas haciéndonos facilitadores de aprendizajes, enriquecimiento así el rol docente.</li>
            </ul>
        `
    },
    {
        "tittle"  :"Metodología",
        "contents":`
            <p>La educación inclusiva implica integralidad y flexibilidad, es decir calidad, pertinencia, dinamización y adaptabilidad de la comunidad de nuestra universidad con su entorno y contexto social, cultural, político y económico.</p>
            <p>Dicho esto, la metodología del curso es flexible, reflexiva y dialógica según vivencias y experiencias de aprendizaje. Este diplomado virtual con tutor, busca motivar su participación y autonomía en la revisión, profundización y aplicación de los conocimientos, lo que conducirá al desarrollo y fortalecimiento de habilidades y capacidades. Por lo cual, lo invitamos a realizar las actividades con el mayor compromiso. Lea cuidadosamente las lecturas base suministradas por su tutor y visualice con atención los videos y recursos creados para usted, pues de esta manera, se asegura la construcción contextualizada de los aspectos aquí propuestos.</p>
            <p>Como parte del desarrollo y reconocimiento de los aprendizajes construidos y vivenciados por cada uno de ustedes, desarrollarán un pre-test que tiene como intención identificar y autor reflexionar en torno a las barreras actitudinales, con la idea de la transformar actitudes e imaginarios en torno a la atención educativa inclusiva con la aplicación y pertinencia del enfoque diferencial, detectando fortalezas y oportunidades de mejoramiento.</p>
            <p>Recuerde que la interacción con compañeros, tutor y recursos, es preponderante para potenciar los aprendizajes, por lo cual, lo invitamos a hacer uso del foro <b>pregúntele a su tutor,</b> allí hallará respuesta a sus preguntas en torno a los aspectos estudiados. De igual forma, le sugerimos explorar y seguir con atención las instrucciones que se dan a lo largo del proceso.</p>
        `
    },
    {
        "tittle"  :"Evaluación",
        "contents":`
            <p>La evaluación en este Diplomado es dialógica y permanente. Responde a un proceso flexible en el que se han planteado actividades en las que la interacción, la resolución de problemas, la práctica y la realimentación permanente contribuyen a la reflexión y la construcción de conocimientos, por lo tanto, al alcance de los REA propuestos.</p>
            <p>El proceso de educación inclusiva implica construcciones dialógicas permanentes, por lo tanto, la evaluación en este diplomado es flexible y conduce a la autoevaluación y autorregulación con una visión de educación diferencial.</p>
            <p>Siendo conocedores de la importancia de los aspectos aquí estudiados y advirtiendo, que este es un diplomado que contribuye positivamente a su formación y su carrera, se ha contemplado un tiempo prudencial para su realización.</p>
            <p>Este diplomado se da por aprobado con la obtención de resultados positivos en el 80% de las actividades propuestas, por lo cual le sugerimos realizar las actividades con compromiso y responsabilidad. A continuación, listamos los ejercicios eje propuestos para cada uno de los REA, pero recuerde que la evaluación es permanente.</p>
            <table border=1 cellspacing=0 cellpadding=2 >
                <tr>
                    <td>REA</td>
                    <td>ACTIVIDADES PROPUESTAS</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>
                        <ul>
                            <li>PRETEST - aproximaciones </li>
                            <li>Expresiones humanas diversas</li>
                            <li>Línea de tiempo - Rutas del lenguaje</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>
                        <ul>
                            <li>Sentir cultural - Folclor Colombiano</li>
                            <li>Galería ser y nosotros</li>
                            <li>La universidad no es universal</li>
                            <li>Encuentro sincrónico</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>
                        <ul>
                            <li>Estudio de situación</li>
                            <li>Propuesta pedagógica disciplinar</li>
                            <li>Postest</li>
                        </ul>
                    </td>
                </tr>
            </table>
        `
    }
]

function main(sym) {

var t=null;
udec = ivo.structure({
        created:function(){
           t=this;
            //precarga audios//
           ivo.load_audio(audios,onComplete=function(){
               ivo(ST+"preload").hide();
               ivo(".label").hide();
               ivo(".len").hide();
            
               t.events();
               t.animation();
               stage1.play().timeScale(1);

           });
        },
        methods: {
         
            events:function(){
                var t=this;
                ivo(".btn").on("click",function(){
                    ivo(".len").hide();
                    let id = parseInt( ivo(this).attr("id").split("Stage_btn_")[1] ) - 1;
                    ivo(ST+"len"+(id+1)).show();
                    stage2.play().timeScale(1);
                    ivo.play("clic");
                    let platilla=`
                        <h1>${textos[id].tittle}</h1>
                        ${textos[id].contents}
                        `;
                    ivo(ST+"texto").html(platilla);
                }).on("mouseover",function(){
                    let id = ivo(this).attr("id").split("Stage_btn_")[1];
                    ivo(".label").hide();
                    ivo(ST+"label_"+id).show();
                })
                .on("mouseout",function(){
                    ivo(".label").hide();
                });

                ivo(ST+"close").on("click",function(){
                    stage2.reverse().timeScale(10);
                    ivo.play("clic");
                });
            },
            animation:function(){
                
                stage1 = new TimelineMax({onComplete:function(){
                    float.play();
                    flores.play();
                }});
                stage1.append(TweenMax.from(ST+"stage_1", .8,        {x:1300,opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"alpha", .8,          {opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"edificio", .8,       {x:1300,opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"guia", .8,           {x:-300,opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"libro", .8,          {x:-300,opacity:0}), 0);
                stage1.append(TweenMax.staggerFrom(".avatars",.4,    {x:100,opacity:0,scaleY:6,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage1.append(TweenMax.from(ST+"flor1", .8,          {x:-300,opacity:0}), 0);
                stage1.append(TweenMax.from(ST+"flor2", .8,          {x:300,opacity:0}), 0);
                stage1.append(TweenMax.staggerFrom(".btn",.4,        {x:100,opacity:0,scaleY:6,y:-600,ease:Elastic.easeOut.config(0.3, 0.4)},.2), 0);
                stage1.stop();

                stage2 = new TimelineMax();
                stage2.append(TweenMax.from(ST+"stage_2", .8,        {x:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"alpha2", .8,         {opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"len", .8,            {x:1300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"Group2", .8,          {opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"hoja_1", .8,         {x:-300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"hoja_2", .8,         {x:300,opacity:0}), 0);
                stage2.append(TweenMax.from(ST+"close", .8,          {x:300,opacity:0, rotation:900, scale:0}), 0);
                stage2.stop();

                
                float = new TimelineMax({repeat:-1,yoyo:true});
                float.append(TweenMax.staggerTo(".btn",5,    {y:23},.5), 0);
                float.stop();

                flores = new TimelineMax({repeat:-1,yoyo:true});
                flores.append(TweenMax.staggerTo(".flores",5,    {rotation:10},.5), 0);
                flores.stop();

            }
        }
 });
}
